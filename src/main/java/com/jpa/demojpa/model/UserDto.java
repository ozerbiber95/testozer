package com.jpa.demojpa.model;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
public class UserDto {
    private String name;
    private String avatar;
    private Set<UserChildDto> userChildren;
}
