package com.jpa.demojpa.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserNewDto {
    private String name;
}
