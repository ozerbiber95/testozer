package com.jpa.demojpa.model;

import lombok.Getter;

import javax.persistence.*;

@MappedSuperclass
@Getter
@SequenceGenerator(name="seq", initialValue=1, allocationSize=30)
public abstract class BaseEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private Long id;

}
