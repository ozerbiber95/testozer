package com.jpa.demojpa.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NamedEntityGraph(name = "joined", includeAllAttributes = true)
public class User extends BaseEntity {
    private String name;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String avatar;
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval = true)
    @JsonManagedReference
    Set<UserChild> userChildren;
    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true,fetch = FetchType.LAZY)
    @JoinColumn(name = "userOneId")
    UserOne userOne;

}
