package com.jpa.demojpa.repository;

import com.jpa.demojpa.model.UserChild;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserChildRepository extends JpaRepository<UserChild, Long> {
}
