package com.jpa.demojpa.repository;

import com.jpa.demojpa.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @EntityGraph(attributePaths = {"userChildren.childName"})
    List<User> findAllByIdNotNull();

    User findByName(String name);
}
