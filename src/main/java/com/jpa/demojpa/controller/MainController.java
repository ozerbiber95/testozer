package com.jpa.demojpa.controller;

import com.jpa.demojpa.model.User;
import com.jpa.demojpa.model.UserDto;
import com.jpa.demojpa.model.UserNewDto;
import com.jpa.demojpa.repository.UserChildRepository;
import com.jpa.demojpa.repository.UserRepository;
import com.jpa.demojpa.util.ModelMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api")
@Transactional
public class MainController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapperUtils modelMapperUtils;
    @PostMapping(path = "/test")
    public ResponseEntity test(){
        List<User> userList=new ArrayList<>();
        User user = new User();
        User user2 = new User();
        userList.add(user);
        userList.add(user2);
        userRepository.saveAll(userList);
        return ResponseEntity.ok().build();
    }
    @PostMapping
    public ResponseEntity saveChild(@RequestBody User user){
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }
    @GetMapping
    public List<UserDto> getChild(){
        List<User> userList=userRepository.findAllByIdNotNull();
       return modelMapperUtils.convertList(userList,UserDto.class);
    }
    @GetMapping(path = "/{userName}")
    public UserNewDto getChild(@PathVariable String userName){
        return modelMapperUtils.convertObject(userRepository.findByName(userName), UserNewDto.class);
    }
}
